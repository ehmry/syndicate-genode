// SPDX-FileCopyrightText: ☭ 2022 Emery Hemingway
// SPDX-License-Identifier: AGPL-3.0-or-later

/**
 * A Terminal service component for exchanging Syndicate packets.
 */

#include <libc/component.h>
	// TODO: until the Nim standard library is liberated
	// from POSIX assume that calling any Nim proc
	// requires a Libc context.

#include <terminal_session/terminal_session.h>
#include <base/rpc_server.h>

extern "C" {
void syndicate_terminal_read_avail_sigh(void *state, Genode::Signal_context_capability);
bool syndicate_terminal_avail(void *state);
size_t syndicate_terminal_write(void *state, size_t length);
size_t syndicate_terminal_read(void *state, size_t length);
}

namespace Terminal {
	using namespace Genode;
	struct Session_component;
}

struct Terminal::Session_component
: Rpc_object<Terminal::Session, Session_component>
{
	void *_state;

	Dataspace_capability const _ds_cap;

	Session_component(void *state, Dataspace_capability ds)
	: _state(state), _ds_cap(ds)
	{ }

	Size size() override { return Size(0, 0); }

	bool avail() override
	{
		bool result = false;
		Libc::with_libc([this, &result] () {
			result = syndicate_terminal_avail(_state);
		});
		return result;
	}

	Genode::size_t _read(Genode::size_t length)
	{
		size_t result = 0;
		Libc::with_libc([this, length, &result] () {
			result = syndicate_terminal_write(_state, length);
		});
		return result;
	}

	Genode::size_t _write(Genode::size_t length)
	{
		size_t result = 0;
		Libc::with_libc([this, length, &result] () {
			result = syndicate_terminal_read(_state, length);
		});
		return result;
	}

	Dataspace_capability _dataspace() { return _ds_cap; }

	void read_avail_sigh(Signal_context_capability cap) override
	{
		Libc::with_libc([this, cap] () {
			syndicate_terminal_read_avail_sigh(_state, cap);
		});
	}

	void size_changed_sigh(Signal_context_capability) override { }

	void connected_sigh(Signal_context_capability sigh) override
	{
		/*
		 * Immediately reflect connection-established signal to the
		 * client because the session is ready to use immediately after
		 * creation.
		 */
		Genode::log("send connected signal to terminal client");
		Signal_transmitter(sigh).submit();
	}

	size_t read(void *, size_t) override { return ~0UL; }
	size_t write(void const *, size_t) override { return ~0UL; }
};
