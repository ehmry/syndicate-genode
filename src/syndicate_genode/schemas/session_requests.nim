
import
  std/typetraits, preserves, std/tables

type
  Space*[E] {.preservesRecord: "space".} = ref object
    `attrs`*: SpaceAttrs[E]

  LocationAttrs*[E] {.preservesDictionary.} = ref object
    `width`*: BiggestInt
    `height`*: BiggestInt
    `xpos`*: BiggestInt
    `ypos`*: BiggestInt

  Args* = Table[Symbol, string]
  UpgradeAttrs*[E] {.preservesDictionary.} = ref object
    `id`*: BiggestInt
    `ram_quota`*: BiggestInt
    `cap_quota`*: BiggestInt

  CreateAttrs*[E] {.preservesDictionary.} = ref object
    `id`*: BiggestInt
    `service`*: string
    `label`*: string

  SpaceAttrs*[E] {.preservesDictionary.} = ref object
    `width`*: BiggestInt
    `height`*: BiggestInt

  UpgradeRequest*[E] {.preservesRecord: "upgrade".} = ref object
    `attrs`*: UpgradeAttrs[E]

  CloseAttrs*[E] {.preservesDictionary.} = ref object
    `id`*: BiggestInt

  CreateRequest*[E] {.preservesRecord: "create".} = ref object
    `attrs`*: CreateAttrs[E]
    `args`*: Args
    `affinity`*: Affinity[E]

  Affinity*[E] {.preservesRecord: "affinity".} = ref object
    `space`*: Space[E]
    `location`*: Location[E]

  CloseRequest*[E] {.preservesRecord: "close".} = ref object
    `attrs`*: CloseAttrs[E]

  Location*[E] {.preservesRecord: "location".} = ref object
    `attrs`*: LocationAttrs[E]

proc `$`*[E](x: Space[E] | LocationAttrs[E] | UpgradeAttrs[E] | CreateAttrs[E] |
    SpaceAttrs[E] |
    UpgradeRequest[E] |
    CloseAttrs[E] |
    CreateRequest[E] |
    Affinity[E] |
    CloseRequest[E] |
    Location[E]): string =
  `$`(toPreserve(x, E))

proc encode*[E](x: Space[E] | LocationAttrs[E] | UpgradeAttrs[E] |
    CreateAttrs[E] |
    SpaceAttrs[E] |
    UpgradeRequest[E] |
    CloseAttrs[E] |
    CreateRequest[E] |
    Affinity[E] |
    CloseRequest[E] |
    Location[E]): seq[byte] =
  encode(toPreserve(x, E))

proc `$`*(x: Args): string =
  `$`(toPreserve(x))

proc encode*(x: Args): seq[byte] =
  encode(toPreserve(x))
