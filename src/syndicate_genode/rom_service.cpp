// SPDX-FileCopyrightText: ☭ 2022 Emery Hemingway
// SPDX-License-Identifier: AGPL-3.0-or-later

/**
 * A ROM service component that propagates Syndicate assertions.
 */

#include <libc/component.h>
	// TODO: until the Nim standard library is liberated
	// from POSIX assume that calling any Nim proc
	// requires a Libc context.

#include <rom_session/rom_session.h>
#include <base/rpc_server.h>

extern "C" {
void syndicate_rom_update(void *state);
void syndicate_rom_sigh(void *state, Genode::Signal_context_capability sigh);
}

namespace Rom {
	using namespace Genode;
	struct Session_component;
}

struct Rom::Session_component : Genode::Rpc_object<Genode::Rom_session>
{
	void *_state;

	Dataspace_capability const ds_cap;

	Session_component(void *state, Dataspace_capability ds)
	: _state(state), ds_cap(ds)
	{ }

	Rom_dataspace_capability dataspace() override {
		return static_cap_cast<Rom_dataspace>(ds_cap); }

	bool update() override { syndicate_rom_update(_state); }

	void sigh(Genode::Signal_context_capability sigh) override {
		syndicate_rom_sigh(_state, sigh); }
};
