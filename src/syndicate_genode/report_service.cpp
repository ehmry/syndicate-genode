// SPDX-FileCopyrightText: ☭ 2022 Emery Hemingway
// SPDX-License-Identifier: AGPL-3.0-or-later

/**
 * A Report service component that propagates reports into Syndicate.
 */

#include <libc/component.h>
	// TODO: until the Nim standard library is liberated
	// from POSIX assume that calling any Nim proc
	// requires a Libc context.

#include <report_session/report_session.h>
#include <base/rpc_server.h>

extern "C" {
void syndicate_report_submit(void *state, size_t length);
}

namespace Report {
	using namespace Genode;
	struct Session_component;
}

struct Report::Session_component : Genode::Rpc_object<Report::Session>
{
	void *_state;

	Dataspace_capability const ds_cap;

	Session_component(void *state, Dataspace_capability ds)
	: _state(state), ds_cap(ds)
	{ }

	Dataspace_capability dataspace() override { return ds_cap; }

	void submit(size_t length) override
	{
		Libc::with_libc([this, &length] () {
			syndicate_report_submit(_state, length);
		});
	}

	void response_sigh(Genode::Signal_context_capability) override
	{
		Genode::warning("Report client called ", __func__);
	}

	size_t obtain_response() override
	{
		Genode::warning("Report client called ", __func__);
		return 0;
	}
};
