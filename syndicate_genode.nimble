# Package

version       = "20230424"
author        = "Emery Hemingway"
description   = "Genode Syndicate components"
license       = "AGPL-3.0-or-later.txt"
srcDir        = "src"
bin           = @["syndicate_service"]


# Dependencies

requires "nim >= 1.6.12", "genode", "syndicate"
