# syndicate-genode

Experimental [Genode](https://genode.org/) [ROM](https://genode.org/documentation/genode-foundations/23.05/components/Common_session_interfaces.html#Read-only_memory__ROM_) and [Report](https://genode.org/documentation/genode-foundations/23.05/components/Common_session_interfaces.html#Report) services backed by [Syndicate](https://syndicate-lang.org/).

Only [tested](https://gitea.c3d2.de/ehmry/sigil/src/branch/trunk/tests/syndicate.nix) with the Sigil fork of Genode.

[![asciicast](https://asciinema.org/a/531107.svg)](https://asciinema.org/a/531107)
